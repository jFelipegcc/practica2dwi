
package mx.edu.uteq.controller;

import java.util.List;import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;


@Controller
public class Home {
  
    
    
    @GetMapping("/home")
    public String pageHome(Model model) {
        return "home";        
    }
    
    @GetMapping("/")
    public String pageIndex(Model model) {
        return "home";        
    }
    
    
}
